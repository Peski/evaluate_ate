
// MRPT dependents
#include "mrpt_scene.hpp"

// STL
#include <algorithm>
#include <cmath>
#include <iostream>
#include <iterator>
#include <limits>
#include <utility>
#include <vector>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "svi_benchmark.hpp"

std::vector<std::pair<int, int>> associate(const svi_benchmark::Trajectory &t1, const svi_benchmark::Trajectory &t2, double th = 1e-3) {

    std::vector<std::pair<int, int>> matches;
    for (int i = 0, n = t1.size(); i < n; ++i) {
        const svi_benchmark::trajectory_t &dt1 = t1[i];
        auto it2 = std::lower_bound(t2.begin(), t2.end(), dt1);

        if (it2 != t2.end() && std::abs(dt1.timestamp - it2->timestamp) < th) {
            matches.push_back(std::make_pair(i, std::distance(t2.begin(), it2)));
            continue;
        }

        if (it2 == t2.begin()) continue;
        auto it1 = std::prev(it2);
        if (std::abs(dt1.timestamp - it1->timestamp) < th)
            matches.push_back(std::make_pair(i, std::distance(t2.begin(), it1)));
    }

    return matches;
}

template<typename T>
struct RMSE {

    RMSE() : mean(0), num(1) { }

    void operator()(T n) { mean = mean + (n - mean) / num++; }

    double get() { return std::sqrt(mean); }

private:
    T mean, num;
};

using RMSEf = RMSE<float>;
using RMSEd = RMSE<double>;

int main(int argc, char* argv[]) {

    if (argc != 3) {
        std::cout << "Usage: evaluate_ate <trajectory> <groundtruth>" << std::endl;
        return -1;
    }

    using namespace svi_benchmark;

    Trajectory t1 = read_file<trajectory_t>(argv[1]);
    Trajectory t2 = read_file<trajectory_t>(argv[2]);

    std::vector<std::pair<int, int>> matches = associate(t1, t2);

    COUT_LOG(t1.size());
    COUT_LOG(matches.size());

    // Estimate Rigid motion Eigen
    int n = matches.size();
    Eigen::Matrix3Xd src(3, n), dst(3, n);
    for (int i = 0; i < n; ++i) {
        const std::pair<int, int> &match = matches[i];

        pose_t dt1 = t1[match.first].pose;
        src.col(i) = Eigen::Vector3d(dt1.tx, dt1.ty, dt1.tz);

        pose_t dt2 = t2[match.second].pose;
        dst.col(i) = Eigen::Vector3d(dt2.tx, dt2.ty, dt2.tz);
    }
    Eigen::Matrix4d T_m = Eigen::umeyama(src, dst, false);
    Eigen::Isometry3d T;
    T.linear() = T_m.block(0, 0, 3, 3);
    T.translation() = T_m.block(0, 3, 3, 1);

    MRPTScene scene;

    // Compute squared errors
    std::vector<double> squared_errors;
    for (const std::pair<int, int> &match : matches) {
        Eigen::Isometry3d P1 = T * t1[match.first].pose,
                P2 = t2[match.second].pose;
        Eigen::Isometry3d E = P2.inverse() * P1;
        squared_errors.push_back(E.translation().squaredNorm());

        scene.addPose(P1.matrix(), 255, 0, 0);
        scene.addPose(P2.matrix(), 0, 0, 255);
    }

    RMSEd rmse = std::for_each(squared_errors.begin(), squared_errors.end(), RMSEd());
    std::cout << "RMSE: " << rmse.get() << std::endl;

    // Show trajectories
    scene.waitForKey();

    return 0;
}
