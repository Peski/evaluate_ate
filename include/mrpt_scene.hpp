
// STL
#include <cstdint>
#include <vector>

// MRPT
#include <mrpt/gui/CDisplayWindow3D.h>
#include <mrpt/opengl/COpenGLScene.h>
#include <mrpt/opengl/CFrustum.h>
#include <mrpt/opengl/CPointCloud.h>
#include <mrpt/opengl/stock_objects.h>

#include <mrpt/math/CMatrixFixedNumeric.h>
#include <mrpt/poses/CPose3D.h>

// Eigen
#include <Eigen/Core>

#include "macros.hpp"

class MRPTScene {

public:

    MRPTScene()
        : win("MRPT Scene", 1920, 1080), point_cloud(mrpt::opengl::CPointCloud::Create()) {

        reset();
    }

    virtual ~MRPTScene() { }

    void addPose(Eigen::Matrix4d T, std::uint8_t r = 255, std::uint8_t g = 0, std::uint8_t b = 0) {
        mrpt::opengl::COpenGLScenePtr scene = win.get3DSceneAndLock();
        mrpt::opengl::CFrustumPtr frustum = mrpt::opengl::CFrustum::Create();

//        mrpt::poses::CPose3D correction;
//        correction.setYawPitchRoll(EIGEN_PI / 2.0, EIGEN_PI / 2.0, 0.0);

        mrpt::math::CMatrixDouble44 m(T);
        mrpt::poses::CPose3D pose(m);
//        frustum->setPose(correction + pose);
        frustum->setPose(pose);
        frustum->setColor_u8(r, g, b);
        frustum->setScale(0.01f);

        scene->insert(frustum);
        win.unlockAccess3DScene();
        win.repaint();

        frustums.push_back(frustum);
    }

    void addPoint(Eigen::Vector3d p) {
        TODO("Add colour support");
        win.get3DSceneAndLock();
        point_cloud->insertPoint(p(0), p(1), p(2));

        win.unlockAccess3DScene();
        win.repaint();
    }

    void clear() {
        TODO("Add clear single points/poses");
        mrpt::opengl::COpenGLScenePtr scene = win.get3DSceneAndLock();
        scene->clear();
        win.unlockAccess3DScene();
        win.repaint();

        point_cloud->clear();
        frustums.clear();

        reset();
    }

    bool isOpen() {
        return win.isOpen();
    }

    int waitForKey() {
        return win.waitForKey();
    }

private:

    mrpt::gui::CDisplayWindow3D win;
    mrpt::opengl::CPointCloudPtr point_cloud;
    std::vector<mrpt::opengl::CFrustumPtr> frustums;

    void reset() {
        point_cloud->setPointSize(3.f);
        point_cloud->enableColorFromX();

        mrpt::opengl::COpenGLScenePtr scene = win.get3DSceneAndLock();
        scene->insert(mrpt::opengl::stock_objects::CornerXYZSimple());
        scene->insert(point_cloud);
        win.unlockAccess3DScene();
    }
};
